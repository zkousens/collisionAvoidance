import threading
import json
import os
import threading
import socket
import time
import util
from boltons import socketutils
#Nathaniel Hanson
#2/13/2018
#Connection.py
#Contains class and methods for establishing a socket to dronology on default port 1234. The method "_work" checks conntection status and builds a message queue.
#Personal modification:Message Queue is redefined at every instantiation of _work to effectively pop messages from the queue
#Reused code from Professor Huang and assignment #3 directory
class Connection:
    _WAITING = 1
    _CONNECTED = 2
    _DEAD = -1

    def __init__(self, msg_queue=[], addr='localhost', port=1234, g_id='default_groundstation'):
        self._g_id = g_id
        self._msgs = msg_queue
        self._addr = addr
        self._port = port
        self._sock = None
        self._conn_lock = threading.Lock()
        self._status = Connection._WAITING
        self._status_lock = threading.Lock()
        self._msg_buffer = ''

    def get_status(self):
        with self._status_lock:
            return self._status

    def set_status(self, status):
        with self._status_lock:
            self._status = status

    def is_connected(self):
        return self.get_status() == Connection._CONNECTED

    def start(self):
        threading.Thread(target=self._work).start()

    def stop(self):
        self.set_status(Connection._DEAD)

    def send(self, msg):
        success = False
        with self._conn_lock:
            if self._status == Connection._CONNECTED:
                try:
                    self._sock.send(msg)
                    self._sock.send(os.linesep)
                    success = True
                except Exception as e:
                    print('failed to send message! ({})'.format(e))

        return success


    def _work(self):
        """
        Main loop.
            1. Wait for a connection
            2. Once connected, wait for commands from dronology
            3. If connection interrupted, wait for another connection again.
            4. Shut down when status is set to DEAD
        :return:
        """
        cont = True
	self._msgs=[]
        while cont:
            status = self.get_status()
            if status == Connection._DEAD:
                # Shut down
                cont = False
            elif status == Connection._WAITING:
                # Try to connect, timeout after 10 seconds.
                try:
                    sock = socket.create_connection((self._addr, self._port),timeout=25.0)
                    self._sock = socketutils.BufferedSocket(sock)
                    handshake = json.dumps({'type': 'connect', 'uavid': self._g_id})
                    self._sock.send(handshake)
                    self._sock.send(os.linesep)
                    self.set_status(Connection._CONNECTED)
                except socket.error as e:
                    print('Socket error ({})'.format(e))
                    time.sleep(10.0)
            else:
                # Receive messages
                try:
                    msg = self._sock.recv_until(os.linesep, timeout=10.0)
                    if self._msgs is not None:
			print("Adding message to queue")
                        self._msgs.append(msg)
			cont=False
                except socket.timeout:
                    pass
                except socket.error as e:
                    print('connection interrupted! ({})'.format(e))
                    self._sock.shutdown(socket.SHUT_RDWR)
                    self._sock.close()
                    self._sock = None
                    self.set_status(Connection._WAITING)
                    time.sleep(20.0)
	return self._msgs

        if self._sock is not None:
            print('Shutting down socket.')
            self._sock.shutdown(socket.SHUT_WR)
            print('Closing socket.')
            self._sock.close()
            return
