import dronekit_sitl
import dronekit
from dronekit import connect, VehicleMode, LocationGlobalRelative, LocationGlobal
import json
import argparse
import os
import threading
import time
import signal
import util
import Connection
import logging 
import math

#Nathaniel Hanson and Zak Kousens
#2/13/2018
#main.py
#Contains main execution to interface with Dronology Server and ArduPilot SITL envrionment. As an interfacing program, the code initiates SITL vehicles and based on configurations passed to it in command line arguments. The code then establishes two threads: one to monitor a message queue for JSON commands from Dronology and the other to periodically send state information to Dronology. SIGINT and SIGTERM are trapped to allow for the code to semi-gracefully clean up before exiting.

#Based off code from Professor Huang and T.A. Sean Bayley

#####Logging Information#####
_LOG = logging.getLogger(__name__)
_LOG.setLevel(logging.INFO)

fh = logging.FileHandler('main.log', mode='w')
fh.setLevel(logging.INFO)
formatter = logging.Formatter('| %(levelname)6s | %(funcName)8s:%(lineno)2d | %(message)s |')
fh.setFormatter(formatter)
_LOG.addHandler(fh)

##### GLOBAL VARIABLES #####
DO_CONT = False
ARDUPATH = os.path.join('/', 'home', 'nathaniel', 'ardupilot')
sitl=None
VID=None
vehicles=[]
routes=[]
sitls=[]
#Lock to protect the waypoint list
lock=threading.Lock()

dronealt={20:0,25:0,30:0,35:0,40:0}
#Function to Load JSON File
def load_json(path2file):
    d = None
    try:
        with open(path2file) as f:
            d = json.load(f)
    except Exception as e:
        exit('Invalid path or malformed json file! ({})'.format(e))

    return d

#Function is capable of instantiating multiple drones
def connect_vehicle(instance,home):
    #Define the new home point
    homep = ','.join(map(str, tuple(home) + (0,0)))
    sitl_defaults = os.path.join(ARDUPATH, 'Tools', 'autotest', 'default_params', 'copter.parm')
    sitl_args = ['-I{}'.format(instance), '--home', homep, '--model', '+', '--defaults', sitl_defaults]
    sitl = dronekit_sitl.SITL(path=os.path.join(ARDUPATH, 'build', 'sitl', 'bin', 'arducopter'))
    #Initialize SITL Environment
    sitl.launch(sitl_args, await_ready=True) 
    #Initialize the Vehicle
    tcp, ip, port = sitl.connection_string().split(':')
    port = str(int(port) + instance * 10)
    conn_string = ':'.join([tcp, ip, port])

    vehicle = dronekit.connect(conn_string)
    vehicle.wait_ready(timeout=10)
    #Redefine the home point to avoid setting the craft to middle of Atlantic Ocean
    print("Successfully added my drone")
    return vehicle, sitl


def get_vehicle_id(i):
    return 'drone{}'.format(i)

#Thread 1: Send State Information to Dronology
def send_state(dronology, vehicles):
    while DO_CONT:
        for i, v in enumerate(vehicles):
            state = util.StateMessage.from_vehicle(v, get_vehicle_id(i))
            dronology.send(str(state))

        time.sleep(1.0)

def arm_vehicle(v):
    print("Basic pre-arm checks")
    # Don't try to arm until autopilot is ready
    while not v.is_armable:
	print(" Waiting for vehicle to initialise...")
	time.sleep(1)
    print("Arming motors")
    # Copter should arm in GUIDED mode
    v.mode = VehicleMode("GUIDED")
    v.armed = True
    # Confirm vehicle armed before attempting to take off
    while not v.armed:
	print(" Waiting for arming...")
	time.sleep(1)
    return v

#Thread 2: Send Dronology Commands to SITL
def command_handle(dronology,vehicles,waypoints): 
    while DO_CONT:
	msq=dronology._work()
	print msq
	if msq is not None:
	    mrecv=msq.pop(0)
	print("Message Recieved")
	mrecv=json.loads(mrecv)
	comm=mrecv['command']
	print mrecv
	print comm
	data=mrecv['data']		
	VID=mrecv['uavid']
	print(VID)
	VID=VID[5:]
	print(VID)
	v=vehicles[int(VID)]	
	#####Set New Waypoint#####
	if comm=="gotoLocation":
		x=float(data['x'])
		y=float(data['y'])
		z=float(data['z'])
		point1 = LocationGlobalRelative(x,y,z)
		with lock:
			waypoints[int(VID)].append(point1)
		v.simple_goto(point1)
	#####Arm Copter######
	elif comm=="setArmed":
		v=arm_vehicle(v)
	#####Set Groundspeed#####
	elif comm=="setGroundspeed":
		speed=float(data['speed'])
		v.groundspeed=speed
	#####Set Home#####
	elif comm=="setHome":
		x=float(data['x'])
		y=float(data['y'])
		z=float(data['z'])
		point1 = LocationGlobalRelative(x,y,z)
		v.home_location=point1
	#####Set Mode#####		
	elif comm=="setMode":
		mode=str(data['mode'])
		v.mode = VehicleMode(mode)
	#####Takeoff#####
	elif comm=="takeoff":
		alt=int(data['altitude'])
		v=arm_vehicle(v)
		v.simple_takeoff(alt)
	#####If Deformed Message or Error#####
	else:
		print("Error: Unknown Command")

        time.sleep(1.0)

# Generates a parametric model for the route of a UAV
# A parametric model is ideal because we do not care if the routes of
# two UAVs intersect, we only care if they intersect at the same time
def parametricPlot(vehicle):
        #For simplicity, all distances are measured relative to a 
        # fixed point on the field
        # Define the x,y,z positon of the UAV
        xpos = relXDist(vehicle.location.global_relative_frame.lon)
        ypos = relYDist(vehicle.location.global_relative_frame.lat)
        zpos = vehicle.location.global_relative_frame.alt
        # Define the x,y,z velocity of the UAV
        xvel = vehicle.velocity.x
        yvel = vehicle.velocity.y
        zvel = vehicle.velocity.z
        # Define arrays to store position of UAV each 1/10th of a second
        xarr[600]
        yarr[600]
        zarr[600]
        # Global time variable, used to monitor possible collision (1/10th s)
        t = GTIME
        # Iterate through time 
        while (t < 600):
                xpos = xpos + xvel/10
                ypos = ypos + yvel/10
                zpos = zpos + zvel/10
                xarr[t] = xpos
                yarr[t] = ypos
                zarr[t] = zpos
                t += 1
        posArrays = [xarr,yarr,zarr]
        return posArrays

def relXDist(xLoc):
	xdist = distAndAngle(xLoc,home[1],0,0,'d')
	return xdist

def relYDist(yLoc):
	ydist = distAndAngle(yLoc,home[2],0,0,'d')
	return ydist

# Check all of the points along the route of two vehicles to see if they crash
def checkRoutes(vehicle1,vehicle2):
        v1Positions = parametricPlot(vehicle1)
        v2Positions = parametricPlot(vehicle2)
        # Return the time a collision occured
        colTime = checkCollision(v1Positions,v2Positions)
        # If a collision is found, modify the route of the vehicle
	return colTime


def checkcollision(positions1,positions2):
        # Import the position arrays
        xarr1 = vehicle1[1]
        yarr1 = vehicle1[2]
        zarr1 = vehicle1[3]
        xarr2 = vehicle2[1]
        yarr2 = vehicle2[2]
        zarr2 = vehicle2[3]

        while (t < 600):
                # If the distance between all 3 coordinates is less than 3m
                # at one instance in time, return the angle that
                # collision occured
                if (distAndAngle(xarr1[t],xarr2[t],yarr1[t],yarr2[t],'d') < 3):
                        # Calculate the trajectory angle for each drone
			angle1 = distAndAngle(xarr1[t],xarr1[t-1],yarr1[t],yarr1[t-1],'a')
                        angle2 = distAndAngle(xarr2[t],xarr2[t-1],yarr2[t],yarr2[t-1],'a')
			if(angle1 > math.pi):
				angle1 = angle1 - math.pi
			if(angle2 > math.pi):
				angle2 = angle2 - math.pi
			# If the trajectories are within ~10 degrees,
			# Tell the collision avoidance function to use
			# the altitude avoidance method
			if(angle2-angle1 < .2 or angle2-angle1 > -.2):
				print("Collisions Detected")
				return 2
			else:
				print("Collisions Detected")
				return 1
        # If no collisions occur, return 0
        return 0

# Distance function, based on examples found on stack exchange
def distAndAngle(xpos1,xpos2,ypos1,ypos2,distOrAngle):
	# Radius of Earth in meters
	R = 6371000

	#Set up the gps coordinates
	lat1 = radians(xpos1)
	lon1 = radians(ypos1)
	lat2 = radians(xpos2)
	lon2 = radians(ypos2)

	dlon = lon2 - lon1
	dlat = lat2 - lat1
	
	# Use Haversine function to calculate distance
	angle = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
	c = 2 * atan2(sqrt(a), sqrt(1 - a))

	distance = R * c
	if (distOrAngle == 'd'):
		return distance
	elif (distOrAngle == 'a'):
		return a

#Thread 3: Check If Drone is at point and send command to go to waypoint
def update_way(dronology,vehicles,waypoints):
	#First Check for and handle collisions
	collisionavoid(dronology,vehicles,waypoints)	
	with lock:
		for v,drone in enumerate(vehicles):
			#If the vehicle is slow/stopped and there is still a point to go to in the list
			if drone.groundspeed<1 and waypoints[v]:
				nextpoint=waypoint[v].pop(0)
				drone.simplegoto(nextpoint)
				drone.groundspeed=10
				print("Going to the next point")
	

def collisionavoid(dronology,vehicles,waypoints):
	for v, drone in enumerate(vehicles):
		for x, drone2 in enumerate(vehicles):
			#If the option is set to zero, no collision is detected, continue normally
			opt=0
			#Confirm that the two drones are not equal to each other
			if v is not x:
				print("Checking for Collisions...")
				opt=checkRoutes(drone,drone2)
			#If the drones are not equal to each other and collision is detected
			#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
			#Case 1: Try to adjust speed to avoid collision
			if opt==1:
				print("Attempting Speed Adjustment...")
				drone.groundspeed=drone.groundspeed*0.75
				drone2.groundspeed=drone.groundspeed*1.25
				#check again to see if collision is avoided
				opt=checkRoutes(drone,drone2)
				if opt is not 0:
					drone.groundspeed=10 #Default value
					opt=2
			#Case 2: Try to adjust to new altitude within the flight lanes		
			if opt==2:
				print("Attempting Altitude Adjustment...")
				#Get initial drone location parameters
				mylat=float(drone.location.lat)
				mylon=float(drone.location.lon)
				myalt=float(drone.location.alt)
				#Find nearest flight altitude possibility
				minalt=20 #Range between min and max altitudes 
				possible=False
				for i in range (20,40,5):
					if abs(myalt-i)<minalt and dronealt[i]<4 and drone2.alt!=i+5 and drone2.alt!=i-5:
						minalt=i #We found a new min altitude
						possible=True
				if possible:
					dronealt[str(minalt)]+=1 #Indicate we have another vehicle in the lane
					myalt=float(minalt)
					#Add the new intermediate point basically just an altitude to the drone's list
					point1 = LocationGlobalRelative(mylat,mylon,myalt)		
					waypoints[v].insert(0,point1)
					#Go to the new altitude					
					drone.simplegoto(point1)
				else:
					#Hope and pray option 3 works
					opt=3
			#Case 3: Make intermediate points for drones to go to, worst case scenario
			if opt==3:
				print("Calculating Intermediate Points...")
				pi=math.pi
				#Plot alternate point to adjust
				#Get waypoints for drone 1 and calculate midwaypoints:
				with lock:
					d1=waypoints[v]
				midy1=float((drone.location.lat-d1.lat)/2)
				midx1=float((drone.location.lon-d1.lon)/2)
				#Get waypoints for drone 2:
				d2=waypoints[x]
				midy2=float((drone2.location.lat-d2.lat)/2)
				midx2=float((drone2.location.lon-d2.lon)/2)
				# Determine angle between points
				theta1=atan(midy1,midx1)
				theta2=atan(midy2,midx2)
				#Get distance between the two points
				r=math.sqrt((midx1)**2+(midy1)**2)
				r2=math.sqrt((midx2)**2+(midy2)**2)
				#Add 22.5 degrees (pi/8 radians) to one path, subtract from the other
				theta1=theta1+(pi/8)
				theta2=theta2-(pi/8)
				#Make new drone points
				newy1=drone.location.lat+r*sin(theta1)
				newx1=drone.location.lon+r*cos(theta1)
				newy2=drone2.location.lat+r*sin(theta2)
				newx2=drone2.location.lon+r*cos(theta2)
				#Add new points to the list
				with lock:
					point1 = LocationGlobalRelative(newy1,newx1,drone.location.alt)		
					waypoints[v].insert(0,point1)
					point2 = LocationGlobalRelative(newy2,newx2,drone2.location.alt)
					waypoints[x].insert(0,point2)	
				#Go to new points				
				drone.simplegoto(point1)
				drone2.simplegoto(point2)
			else:
				print("Welp, you've invented a scenario I cannot handle. Enjoy the implosion/falling battery acid")

##### Main Execution #####
def main(path_to_drone,ardupath=None):
    vehicles=[]
    sitls=[]
    waypoints=[[] for i in range(10)]
    if ardupath is not None:
        global ARDUPATH
        ARDUPATH = ardupath
    
    global DO_CONT
    DO_CONT = True

    mydrone=load_json(path_to_drone)
    dronology = Connection.Connection()
    dronology.start()

    # Define the shutdown behavior
    def stop(*args):
        global DO_CONT
        DO_CONT = False
        print('STOPPING')

        for v,sitl in zip(vehicles,sitls):
            v.close()
	    sitl.stop()

        dronology.stop()
    # Signal Handler Functions for SIGTERM/SIGINT
    signal.signal(signal.SIGINT, stop)
    signal.signal(signal.SIGTERM, stop)
    
    # Start up the drones from the JSON File
    for i, v_config in enumerate(mydrone):
    	home = v_config['home']
    	VID=str(v_config['vehicle_id'])
    	vehicle, sitl = connect_vehicle(i, home)
	handshake = util.DroneHandshakeMessage.from_vehicle(vehicle, get_vehicle_id(i))
 	dronology.send(str(handshake))   
	vehicles.append(vehicle)
	#routes.append(v_config['waypoints'])
    	print("here")

    # Create a thread for sending the state of drones back to Dronology
    w0 = threading.Thread(target=send_state, args=(dronology, vehicles))
    # Create a thread for sending commands to SITL Drones
    w1 = threading.Thread(target=command_handle,args=(dronology,vehicles,waypoints))  
    # Create a thread for checking existing waypoints of drones and sending them to the correct waypoints
    w2 = threading.Thread(target=update_way,args=(dronology,vehicles,waypoints))  
    # Start the threads...
    w0.start()
    # Start command thread but wait for vehicle to initialize
    print("Vehicle Initializing")
    w1.start()
    w2.start()

    #Loop Continuously 
    while DO_CONT:	
        time.sleep(1.0)

#Code Begins Execution Here
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--ardu',type=str, default=ARDUPATH)
    parser.add_argument('-addr', '--address', type=str, default='localhost')
    parser.add_argument('-p', '--port', type=int, default=1234)
    parser.add_argument('-d', '--drone', type=str, default='../cfg/drone_cfgs/default.json')
    args = parser.parse_args()
    main(args.drone, args.ardu)
