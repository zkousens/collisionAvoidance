import dronekit_sitl
import dronekit
from dronekit import connect, VehicleMode, LocationGlobalRelative, LocationGlobal
import json
import argparse
import os
import threading
import time
import signal
import util
import Connection
#Nathaniel Hanson
#2/13/2018
#main.py
#Contains main execution to interface with Dronology Server and ArduPilot SITL envrionment. As an interfacing program, the code initiates SITL vehicles and based on configurations passed to it in command line arguments. The code then establishes two threads: one to monitor a message queue for JSON commands from Dronology and the other to periodically send state information to Dronology. SIGINT and SIGTERM are trapped to allow for the code to semi-gracefully clean up before exiting.

#Based off code from Professor Huang and T.A. Sean Bayley

##### GLOBAL VARIABLES #####
DO_CONT = False
ARDUPATH = os.path.join('/', 'home', 'parallels', 'git', 'ardupilot')
vehicle=None
sitl=None
VID=None
vehicles=[]

#Function to Load JSON File
def load_json(path2file):
    d = None
    try:
        with open(path2file) as f:
            d = json.load(f)
    except Exception as e:
        exit('Invalid path or malformed json file! ({})'.format(e))

    return d

#Function is capable of instantiating multiple drones
def connect_vehicle(i,home):
    #Define the new home point
    homep = ','.join(map(str, tuple(home) + (0,0)))
    sitl_defaults = os.path.join(ARDUPATH, 'Tools', 'autotest', 'default_params', 'copter.parm')
    sitl_args = ['-I{}'.format(i), '--home', homep, '--model', '+', '--defaults', sitl_defaults]
    sitl = dronekit_sitl.SITL(path=os.path.join(ARDUPATH, 'build', 'sitl', 'bin', 'arducopter'))
    #Initialize SITL Environment
    sitl.launch(sitl_args, await_ready=True) 
    #Initialize the Vehicle
    vehicle = dronekit.connect(sitl.connection_string())
    vehicle.wait_ready(timeout=120)
    #Redefine the home point to avoid setting the craft to middle of Atlantic Ocean
    point1 = LocationGlobal(float(home[0]),float(home[1]),0)
    vehicle.home_location=point1
    print("Successfully added my drone")
    return vehicle, sitl


def get_vehicle_id(i):
    return 'drone{}'.format(i)

#Thread 1: Send State Information to Dronology
def send_state(dronology, vehicles):
    while DO_CONT:
        for i, v in enumerate(vehicles):
            state = util.StateMessage.from_vehicle(v, get_vehicle_id(i))
            dronology.send(str(state))

        time.sleep(1.0)

def arm_vehicle(v):
    print("Basic pre-arm checks")
    # Don't try to arm until autopilot is ready
    while not v.is_armable:
	print(" Waiting for vehicle to initialise...")
	time.sleep(1)
    print("Arming motors")
    # Copter should arm in GUIDED mode
    v.mode = VehicleMode("GUIDED")
    v.armed = True
    # Confirm vehicle armed before attempting to take off
    while not v.armed:
	print(" Waiting for arming...")
	time.sleep(1)
    return v

#Thread 2: Send Dronology Commands to SITL
def command_handle(dronology,vehicles): 
    while DO_CONT:
        for i, v in enumerate(vehicles):
		while True:
			msq=dronology._work()
			print msq
			if msq is not None:
			    mrecv=msq.pop(0)   
			    print("Message Recieved")
			    mrecv=json.loads(mrecv)
			    comm=mrecv['command']
			    print mrecv
			    print comm
			    data=mrecv['data']
			    #Handle Incoming Message
		
			    #####Set New Waypoint#####
			    if comm=="gotoLocation":
				x=float(data['x'])
				y=float(data['y'])
				z=float(data['z'])
				point1 = LocationGlobalRelative(x,y,z)
				v.simple_goto(point1)
			    #####Arm Copter######
			    elif comm=="setArmed":
				v=arm_vehicle(v)
			    #####Set Groundspeed#####
			    elif comm=="setGroundspeed":
				speed=float(data['speed'])
				v.groundspeed=speed
			    #####Set Home#####
			    elif comm=="setHome":
				x=float(data['x'])
				y=float(data['y'])
				z=float(data['z'])
				point1 = LocationGlobalRelative(x,y,z)
				v.home_location=point1
			    #####Set Mode#####		
			    elif comm=="setMode":
				mode=str(data['mode'])
				v.mode = VehicleMode(mode)
			    #####Takeoff#####
			    elif comm=="takeoff":
				alt=int(data['altitude'])
			        v=arm_vehicle(v)
				v.simple_takeoff(alt)
			    #####If Deformed Message or Error#####
			    else:
				print("Error: Unknown Command")

        time.sleep(1.0)
###################################################################
# Generates a parametric model for the route of a UAV
# A parametric model is ideal because we do not care if the routes of
# two UAVs intersect, we only care if they intersect at the same time
def parametricPlot(vehicle):
        #For simplicity, all distances are measured relative to a 
        # fixed point on the field
        # Define the x,y,z positon of the UAV
        xpos = relDist(vehicle.location.x)
        ypos = relDist(vehicle.location.y)
        zpos = relDist(vehicle.location.z)
        # Define the x,y,z velocity of the UAV
        xvel = vehicle.velocity.x
        yvel = vehicle.velocity.y
        zvel = vehicle.velocity.z
        # Define arrays to store position of UAV each 1/10th of a second
        xarr[600]
        yarr[600]
        zarr[600]
        # Global time variable, used to monitor possible collision (1/10th s)
        t = GTIME
        # Iterate through time 
        while (t < 600):
                xpos = xpos + xvel/10
                ypos = ypos + yvel/10
                zpos = zpos + zvel/10
                xarr[t] = xpos
                yarr[t] = ypos
                zarr[t] = zpos
                t += 1
        posArrays = [xarr,yarr,zarr]
        return posArrays

# Check all of the points along the route of two vehicles to see if they crash
def checkRoutes(vehicle1,vehicle2):
        v1Positions = parametricPlot(vehicle1)
        v2Positions = parametricPlot(vehicle2)
        # Return the time a collision occured
        colTime = checkCollision(v1Positions,v2Positions)
        # If a collision is found, modify the route of the vehicle
        if (colTime != 0):
                changeRoute(vehicle1, vehicle2, colTime)

def checkCollision(positions1,positions2):
        # Import the position arrays
        xarr1 = vehicle1[1]
        yarr1 = vehicle1[2]
        zarr1 = vehicle1[3]
        xarr2 = vehicle2[1]
        yarr2 = vehicle2[2]
        zarr2 = vehicle2[3]

        while (t < 600):
                # If the distance between all 3 coordinates is less than 3m
                # at one instance in time, return the time that a
                # collision occured
                if (dist(xarr1[t],xarr2[t]) < 3 and dist(yarr1[t],yarr2[t]) < 3):
                        return t
        # If no collisions occur, return 0
        return 0

###################################################################
##### Main Execution #####
def main(path_to_config,path_to_drone,ardupath=None):
    if ardupath is not None:
        global ARDUPATH
        ARDUPATH = ardupath
    
    global DO_CONT
    DO_CONT = True

    config = load_json(path_to_config)
    mydrone=load_json(path_to_drone)
    dronology = Connection.Connection()
    dronology.start()

    # Define the shutdown behavior
    def stop(*args):
        global DO_CONT
        DO_CONT = False
        print('STOPPING')

        for v in vehicles:
            v.close()

        dronology.stop()
    # Signal Handler Functions for SIGTERM/SIGINT
    signal.signal(signal.SIGINT, stop)
    signal.signal(signal.SIGTERM, stop)
    
    # Start up all the singular drone from the JSON File
    for i, v_config in enumerate(mydrone):
    	home = v_config['home']
    	VID=str(v_config['vehicle_id'])
    	vehicle, sitl = connect_vehicle(i, home)
    	print("here")
    	#Send a handshake to the drone
    
    ##### Send Handshake to Drone ######
    handshake = util.DroneHandshakeMessage.from_vehicle(vehicle, get_vehicle_id(i))
    dronology.send(str(handshake))   
    vehicles.append(vehicle)

    # Create a thread for sending the state of drones back to Dronology
    w0 = threading.Thread(target=send_state, args=(dronology, vehicles))
    # Create a thread for sending commands to SITL Drones
    w1 = threading.Thread(target=command_handle,args=(dronology,vehicles))    
    # Start the thread.
    w0.start()
    # Start command thread but wait for vehicle to initialize
    print("Vehicle Initializing")
    time.sleep(30)
    w1.start()
    
    #Loop 
    while DO_CONT:	
        time.sleep(1.0)

#Code Begins Execution Here
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-c','--config', type=str, help='Please enter the configuration file', default='../cfg/global_cfg.json')
    parser.add_argument('-a', '--ardu',type=str, default=ARDUPATH)
    parser.add_argument('-addr', '--address', type=str, default='localhost')
    parser.add_argument('-p', '--port', type=int, default=1234)
    parser.add_argument('-d', '--drone', type=str, default='../cfg/drone_cfgs/default.json')
    args = parser.parse_args()
    main(args.config, args.drone, args.ardu)
