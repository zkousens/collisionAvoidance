import dronekit_sitl
import dronekit
import json
import argparse
import os
import threading
import time
import signal
import util
import logging

_LOG = logging.getLogger(__name__)
_LOG.setLevel(logging.INFO)

fh = logging.FileHandler('main.log', mode='w')
fh.setLevel(logging.INFO)
formatter = logging.Formatter('| %(levelname)6s | %(funcName)8s:%(lineno)2d | %(message)s |')
fh.setFormatter(formatter)
_LOG.addHandler(fh)


DO_CONT = False
GTIME = 0

# make sure you change this so that it's correct for your system 
ARDUPATH = os.path.join('/', 'home', 'parallels', 'git', 'ardupilot')


def load_json(path2file):
    d = None
    try:
        with open(path2file) as f:
            d = json.load(f)
    except Exception as e:
        exit('Invalid path or malformed json file! ({})'.format(e))

    return d


def connect_vehicle(instance, home):
    home_ = tuple(home) + (0,)
    home_ = ','.join(map(str, home_))
    sitl_defaults = os.path.join(ARDUPATH, 'Tools', 'autotest', 'default_params', 'copter.parm')
    sitl_args = ['-I{}'.format(instance), '--home', home_, '--model', '+', '--defaults', sitl_defaults]
    sitl = dronekit_sitl.SITL(path=os.path.join(ARDUPATH, 'build', 'sitl', 'bin', 'arducopter'))
    sitl.launch(sitl_args, await_ready=True)

    tcp, ip, port = sitl.connection_string().split(':')
    port = str(int(port) + instance * 10)
    conn_string = ':'.join([tcp, ip, port])

    vehicle = dronekit.connect(conn_string)
    vehicle.wait_ready(timeout=120)

    return vehicle, sitl


def get_vehicle_id(i):
    return 'drone{}'.format(i)


def state_out_work(dronology, vehicles):
    while DO_CONT:
        for i, v in enumerate(vehicles):
            state = util.StateMessage.from_vehicle(v, get_vehicle_id(i))
            state_str = str(state)
            _LOG.info(state_str)
            dronology.send(state_str)

        time.sleep(1.0)
#################################################################
# Generates a parametric model for the route of a UAV
# A parametric model is ideal because we do not care if the routes of
# two UAVs intersect, we only care if they intersect at the same time
def parametricPlot(vehicle):
	#For simplicity, all distances are measured relative to a 
	# fixed point on the field
	# Define the x,y,z positon of the UAV
	xpos = relDist(vehicle.location.x)
	ypos = relDist(vehicle.location.y)
	zpos = relDist(vehicle.location.z)
	# Define the x,y,z velocity of the UAV
	xvel = vehicle.velocity.x
	yvel = vehicle.velocity.y
	zvel = vehicle.velocity.z
	# Define arrays to store position of UAV each 1/10th of a second
	xarr[600]
	yarr[600]
	zarr[600]
	# Global time variable, used to monitor possible collision (1/10th s)
	t = GTIME
	# Iterate through time 
	while (t < 600):
		xpos = xpos + xvel/10
		ypos = ypos + yvel/10
		zpos = zpos + zvel/10
		xarr[t] = xpos	
		yarr[t] = ypos	
		zarr[t] = zpos	
		t += 1
	posArrays = [xarr,yarr,zarr]
	return posArrays

# Check all of the points along the route of two vehicles to see if they crash
def checkRoutes(vehicle1,vehicle2):
	v1Positions = parametricPlot(vehicle1)
	v2Positions = parametricPlot(vehicle2)
	# Return the time a collision occured
	colTime = checkCollision(v1Positions,v2Positions)
	# If a collision is found, modify the route of the vehicle
	if (colTime != 0):
		changeRoute(vehicle1, vehicle2, colTime)

def checkCollision(positions1,positions2):
	# Import the position arrays
	xarr1 = vehicle1[1]
	yarr1 = vehicle1[2]
	zarr1 = vehicle1[3]
	xarr2 = vehicle2[1]
	yarr2 = vehicle2[2]
	zarr2 = vehicle2[3]
	
	while (t < 600):
		# If the distance between all 3 coordinates is less than 3m
		# at one instance in time, return the time that a
		# collision occured
		if (dist(xarr1[t],xarr2[t]) < 3 and dist(yarr1[t],yarr2[t]) < 3 and dist(zarr1[t],zarr2[t]) < 3):
			return t
	# If no collisions occur, return 0
	return 0


#################################################################
def main(path_to_config, ardupath=None):
    if ardupath is not None:
        global ARDUPATH
        ARDUPATH = ardupath
    
    global DO_CONT
    DO_CONT = True

    config = load_json(path_to_config)
    dronology = util.Connection()
    dronology.start()

    # A list of sitl instances.
    sitls = []
    # A list of drones. (dronekit.Vehicle)
    vehicles = []
    # A list of lists of lists (i.e., [ [ [lat0, lon0, alt0], ...] ...]
    # These are the waypoints each drone must go to!
    routes = []

    # Example:
    # vehicle0 = vehicles[0]
    # waypoints_for_vehicle0 = routes[0]
    # for waypoint in waypoints_for_vehicle0:
    #    lat, lon, alt = waypoint
    #    vehicle0.simple_goto(lat, lon, alt)

    # The above example obviously won't work... you'll need to write some code to figure out when the current waypoint
    # has been reached and it's time to go to the next waypoint.

    # Define the shutdown behavior
    def stop(*args):
        global DO_CONT
        DO_CONT = False
        w0.join()

        for v, sitl in zip(vehicles, sitls):
            v.close()
            sitl.stop()

        dronology.stop()

    signal.signal(signal.SIGINT, stop)
    signal.signal(signal.SIGTERM, stop)
    
    # Start up all the drones specified in the json configuration file
    for i, v_config in enumerate(config):
        home = v_config['start']
        vehicle, sitl = connect_vehicle(i, home)

        handshake = util.DroneHandshakeMessage.from_vehicle(vehicle, get_vehicle_id(i))
        dronology.send(str(handshake))

        sitls.append(sitl)
        vehicles.append(vehicle)
        routes.append(v_config['waypoints'])
        
    # Create a thread for sending the state of drones back to Dronology
    w0 = threading.Thread(target=state_out_work, args=(dronology, vehicles))
    # Start the thread.
    w0.start()

    # At this point, all of the "behind the scenes stuff" has been set up.
    # It's time to write some code that:
    #   1. Starts up the drones (set the mode to guided, arm, takeoff)
    #   2. Sends the drones to their waypoints
    #   3. Hopefully avoids collisions!


    # You're encouraged to restructure this code as necessary to fit your own design.
    # Hopefully it's flexible enough to support whatever ideas you have in mind.


    # wait until ctrl c to exit
    while DO_CONT:
        time.sleep(5.0)


if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument('path_to_config', type=str, help='the path to the drone configuration file.')
    ap.add_argument('--ardupath', type=str, default=ARDUPATH)
    args = ap.parse_args()
    main(args.path_to_config, ardupath=args.ardupath)
